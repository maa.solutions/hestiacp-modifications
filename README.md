# Server setup

## Installation

After installation of the OS, add (optional) the storage:

1. Run `lsblk` to see the path of the attached storage.
1. Run `fdisk /dev/sdb` and create a new partition.
1. Run `pvcreate /dev/sdbN` to create a LVM physical device.
1. Run `vgcreate vg_new_name /dev/sdbN` to create a volume group with the physical device.
1. Run `lvcreate -L xxGB -n lv_new_name vg_new_name` to create a logical volume in the volume group.
1. Create a filesystem on the new logical volume `mkfs.ext4 /dev/vg_new_name/lv_new_name`.
1. Run `blkid` and copy the UUID of the new storage.
1. Add an entry into `/etc/fstab`: `UUID=xxx-xxx-xxx-xxx /home ext4 discard,nofail,defaults 0 0`

Set the name of the serveur :

1. Edit `/etc/hostname` and `/etc/hosts`.
1. Reboot

Install NetData :

Run `bash <(curl -Ss https://my-netdata.io/kickstart.sh)`.

### IP Failovers

- Create a `55-ip-failover.yml` Netplan configuration file with the following content:

```yml
network:
    version: 2
    vlans:
        veth0:                          # or veth1, 2
        id: 0                           # or 1, 2, does correlate to the vethN value
        link: ens3                      # the real network adapter
        dhcp4: no
        addresses: [xxx.xxx.xxx.xxx/24] # IP failover address
```

- Run the command `netplan --debug generate` to test and compile the configuration.
- Run the command `netplan apply` to activate the interface.

**ATTENTION**: If this action is taken after the installation of Hestia, the new IP address needs to be added through the administration under _Server_ > _Network_ with the network mask `255.255.255.0` and the real network adapter!

### Auto updates

- Edit `/etc/apt/apt.conf.d/50unattended-upgrades` and comment the line `Unattended-Upgrade::Allowed-Origins { ... }` ; add a section :

    ```python
    Unattended-Upgrade::Origins-Pattern {
        "o=*";
    };
    ```

- Uncomment and complete the lines :

    - `Unattended-Upgrade::Mail "marcandre.appel@gmail.com";`
    - `Unattended-Upgrade::MailReport "always";`
    - `Unattended-Upgrade::Remove-Unused-Kernel-Packages "true";`
    - `Unattended-Upgrade::Remove-Unused-Dependencies "true";`
    - `Unattended-Upgrade::Automatic-Reboot "true";`

- Create the file `20auto-upgrades` with the following content :

    ```python
    APT::Periodic::Update-Package-Lists "1";
    APT::Periodic::Download-Upgradable-Packages "1";
    APT::Periodic::AutocleanInterval "7";
    APT::Periodic::Unattended-Upgrade "1";
    ```

- Test `unattended-upgrades --dry-run -d` and reload `systemctl restart unattended-upgrades.service`.


## Hestia installation

1. Install Hestia :
    ```sh
    wget https://raw.githubusercontent.com/hestiacp/hestiacp/release/install/hst-install.sh
    bash hst-install.sh -a no -n yes -w yes -o no -v yes -j no -k no -m yes -g no -x yes -z no -c no -t no -i yes -b yes -y yes
    ```

1. Generate a SSH key `ssh-keygen -t rsa -b 4096` and add the key to GitLab.
1. Clone the repo `git clone git@gitlab.com:appelsolutions/hestiacp/modifications.git hestiacp-modifications`.
1. Copy the file `cp hestiacp-modifications/v-add-web-domains-ipv6 /usr/local/hestia/bin/`.
1. Make it executable `chmod +x /usr/local/hestia/bin/v-add-web-domains-ipv6`.
1. Copy the content of the folder `cp -r hestiacp-modifications/nginx/* /usr/local/hestia/data/templates/web/nginx/`.

1. Add **nginx** PPA: `sudo add-apt-repository ppa:ondrej/nginx-mainline`

1. Add **php8.0** (and possibiliy 7.4): `apt install -y php8.0-{common,cli,bcmath,curl,mbstring,zip,bz2,xml,redis,uuid,yaml,mysql,imagick,gd}`.
1. Set it as default: `update-alternatives --set php /usr/bin/php8.0`.
1. Add it to Hestia `v-add-web-php 8.0` (https://forum.hestiacp.com/t/quick-hack-to-enable-ipv6-for-all-vhosts/2439)

1. Download composer `wget https://getcomposer.org/composer-2.phar`.
1. Install composer `mv composer-2.phar /usr/local/bin/composer && chmod +x /usr/local/bin/composer`.

1. Download NodeJS `curl -sL https://deb.nodesource.com/setup_14.x | bash -`.
1. Install NodeJS `apt install -y nodejs`.

1. Install Redis `apt install -y redis-server`.

## Hestia SSL

1. Run `v-list-sys-ips` and `v-list-web-domains` to list all needed informations, and if necessary:
1. Run `v-change-web-domain-ip admin host.domain.name ip.ad.dr.ess`.
1. Finally run `v-add-letsencrypt-host`.

## Hestia & DeployHQ (atomic releases)

1. Create an user (ex.: `user1`) and set the template to the new `deployhq-laravel`.
1. Prepare the `/etc/sudoers.d/99-systemctl-restart-php80-fpm` file with the entry : 
    ```conf
    user1 ALL=NOPASSWD: /bin/systemctl restart php8.0-fpm.service
    ```
1. Add the SSH command to DeployHQ : `sudo restart php8.0-fpm.service` at the end.

### Supervisor

1. Install Supervisor: `sudo apt-get install supervisor`.
1. Create a configuration like `/etc/supervisor/conf.d/horizon.conf` (or project name):

    ```ini
    [program:horizon]
    process_name=%(program_name)s
    command=php /home/user1/website.tld/public_html/current/artisan horizon
    autostart=true
    autorestart=true
    user=user1
    redirect_stderr=true
    stdout_logfile=/home/user/website.tld/logs/horizon.log
    stopwaitsecs=3600
    ```
1. Activate the supervisor:

```sh
sudo supervisorctl reread
sudo supervisorctl update
sudo supervisorctl start horizon
```


On HestiaCP :

setup the cron : `cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1`

## More tools

```sh
wget https://gitlab.com/volian/nala/uploads/dd7631fdacb0c4837df18ed11b8bfe15/nala_0.14.0_all.deb
dpkg -i nala_0.14.0_all.deb
apt install gdu
```

Edit /etc/profile and add:
```sh
alias apt=nala
alias du=gdu
```